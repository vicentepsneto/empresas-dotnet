﻿using System;
using System.Collections.Generic;
using System.Text;
using Empresas.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Empresas.Data
{
    public class EmpresasDbContext : IdentityDbContext
    {
        public EmpresasDbContext(DbContextOptions<EmpresasDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Empresa>()
                .Property(i => i.Id)
                .UseSqlServerIdentityColumn();

            modelBuilder.Entity<Empresa>().ToTable("Empresa");
        }

        public DbSet<Empresa> Empresas { get; set; }
    }
}
