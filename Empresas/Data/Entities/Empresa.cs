﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Empresas.Data.Entities
{
    public class Empresa
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string NomeFantasia { get; set; }

        [Required]
        public string RazaoSocial { get; set; }

        [Required]
        public string CNPJ { get; set; }

        [Required]
        public int TipoEmpresaId { get; set; }

        public string Telefone { get; set; }

        public string Endereco { get; set; }
        /*
         NomeFantasia	varchar(100)	Unchecked
RazaoSocial	varchar(100)	Unchecked
CNPJ	varchar(14)	Unchecked
Telefone	varchar(15)	Checked
Endereco	varchar(50)	Checked*/
    }
}
