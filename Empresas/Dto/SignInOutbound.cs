﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Empresas.Dto
{
    public class SignInOutbound
    {
        [JsonProperty("access-token")]
        public string AccessToken { get; set; }

        [JsonProperty("client")]
        public string Client { get; set; }

        [JsonProperty("uid")]
        public string UId { get; set; }
    }
}
