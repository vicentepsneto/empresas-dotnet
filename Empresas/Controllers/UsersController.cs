﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Empresas.Dto;
using Microsoft.AspNetCore.Mvc;
using Empresas.Data;
using Microsoft.AspNetCore.Identity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Empresas.Controllers
{
    [Route("api/{api_version}/[controller]")]
    public class UsersController : Controller
    {
        private SignInManager<IdentityUser> _signInManager;
        private UserManager<IdentityUser> _userManager;
        private EmpresasDbContext _db;
        
        public UsersController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, EmpresasDbContext context)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _db = context;
        }

        // POST api/{{api_version}}/<controller>/auth/sign_in
        [HttpPost("auth/sign_in")]
        public async Task<ActionResult<SignInOutbound>> SignIn([FromBody]SignInInbound loginData)
        {
            var user = await _userManager.FindByEmailAsync(loginData.Email);
            SignInOutbound result;

            if (user != null)
            {
                var checkPassword = await _signInManager.CheckPasswordSignInAsync(user, loginData.Password, false);
                //await _userManager.CheckPasswordAsync(user, loginData.Password);
                if (checkPassword.Succeeded)
                {
                    result = new SignInOutbound();
                    //_userManager.
                    //_signInManager.
                    //var teste1 = await _signInManager.SignInAsync(user, true, )
                }
                else
                    return BadRequest("Senha inválida");
            }
            else
                return BadRequest("Usuário não encontrado");

            // TODO:

            return result;
        }

    }
}
