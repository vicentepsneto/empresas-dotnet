﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Empresas.Data;
using Empresas.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Empresas.Controllers
{
    //[Authorize]
    [Route("api/{api_version}/enterprises")]
    public class EmpresasController : Controller
    {

        EmpresasDbContext _db;

        public EmpresasController(EmpresasDbContext db)
        {
            _db = db;
        }

        // GET api/{api_version}/enterprises?enterprise_types=1&name=aQm
        [HttpGet]
        public Empresa[] Lista(string api_version, int[] enterprise_types, string name)
        {
            //string types = enterprise_types != null ? string.Join(",", enterprise_types) : null;
            var types = enterprise_types.ToList();

            var emp = _db.Empresas.Where(e => (enterprise_types == null || enterprise_types.Length == 0 || types.Contains(e.TipoEmpresaId)) 
                    && (string.IsNullOrWhiteSpace(name) || e.NomeFantasia == name)).ToArray();
            return emp;
        }

        //// GET api/{api_version}/enterprises
        //[HttpGet]
        //public Empresa[] Lista(string api_version)
        //{
        //    return _db.Empresas.ToArray();
        //}

        [HttpGet("{id}")]
        public Empresa Lista(string api_version, int id)
        {
            var empresa = _db.Empresas.FirstOrDefault(e => e.Id == id);
            return empresa;
        }

    }
}
